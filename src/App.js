import Header from "./components/Header";
import Hero from "./components/Hero";
import VideoSection from "./components/VideoSection";
import OperationTechSection from "./components/OperationsTechSection";
import KnowledgeCostumers from "./components/KnowledgeCostumers";
import TalkToUs from "./components/TalkToUs";

import "./icons";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Header />
      <Hero />
      <VideoSection />
      <OperationTechSection />
      <KnowledgeCostumers />
      <TalkToUs />
      <Footer />
    </div>
  );
}

export default App;
