const colors = {
  white: "#FFFFFF",
  lightPurple: "#7d47cc", //for gradients
  purple: "#2a1844",
  highlightPurple: "#532f88",
  green: "#bada55",
  black: "#000000",
  orange: "#aa4c1b",
};

export default colors;
