import colors from "../../colors";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { animated, useSpring } from "@react-spring/web";
import { useInView } from "react-intersection-observer";

export default function Hero() {
  const [isHovingLearnMore, setIsHovingLearnMore] = useState(false);
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0.1,
  });

  const titleSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(0, 100%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(0, 100%, 0)",
    },
    config: { duration: 800 },
  });

  const subTitleSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(0, 200%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(0, 200%, 0)",
    },
    config: { duration: 800 },
  });

  const buttonSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(-200%, -100%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView
        ? "translate3d(0, 0, 0)"
        : "translate3d(-200%, -100%, 0)",
    },
    config: { duration: 800 },
  });

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundImage: "url(/herobg.webp)",
        height: "80vh",
        position: "relative",
      }}
      ref={ref}
    >
      <div style={{ flex: 1 }}>
        <video
          autoPlay
          loop
          muted
          controls={false}
          style={{
            position: "absolute",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
            objectFit: "cover",
          }}
        >
          <source src="/heroVideo.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </div>
      <div
        style={{
          marginRight: 50,
          color: colors.white,
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
          zIndex: 10,
        }}
      >
        <animated.h1
          style={{
            fontSize: 55,
            ...titleSpring,
            textShadow: "5px 5px 5px rgba(0, 0, 0, 0.3)",
          }}
        >
          Pioneering the Future:
        </animated.h1>

        <animated.h2
          style={{
            fontSize: 22,
            marginTop: 10,
            ...subTitleSpring,
            textShadow: "5px 5px 5px rgba(0, 0, 0, 0.3)",
          }}
        >
          Where Hybrid Intelligence + Gen AI
          <span style={{ color: colors.green }}>
            {" "}
            = Optimal Asset Performance
          </span>
        </animated.h2>
        <animated.button
          style={{
            display: "flex",
            borderRadius: isHovingLearnMore ? 20 : 6,
            transition: "200ms border-radius",
            border: isHovingLearnMore
              ? `1px ${colors.green} solid`
              : `1px ${colors.lightPurple} solid`,
            padding: "5px 10px",
            alignItems: "center",
            marginTop: 30,
            fontStyle: isHovingLearnMore ? "italic" : "normal",
            letterSpacing: 2,
            buttonSpring,
            textShadow: "5px 5px 5px rgba(0, 0, 0, 0.3)",
            fontSize: 22,
          }}
          onMouseEnter={() => setIsHovingLearnMore(true)}
          onMouseLeave={() => setIsHovingLearnMore(false)}
        >
          Learn more
          <FontAwesomeIcon
            icon="fa-solid fa-right-long"
            size="1x"
            color={colors.green}
            style={{ marginLeft: 5 }}
          />
        </animated.button>
      </div>
    </div>
  );
}
