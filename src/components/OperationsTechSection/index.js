import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import colors from "../../colors";
import { useState } from "react";
import data from "./data";
import { animated, useSpring } from "@react-spring/web";
import { useInView } from "react-intersection-observer";

export default function OperationTechSection() {
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0.1,
  });

  const titleSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(0, 100%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(0, 100%, 0)",
    },
    config: { duration: 800 },
  });

  return (
    <div
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
      ref={ref}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: 640,
          marginRight: 160,
        }}
      >
        <animated.h1
          style={{
            marginTop: 110,
            fontSize: 50,
            lineHeight: 1.5,
            fontWeight: 400,
            ...titleSpring,
          }}
        >
          Optimize your business with engineering intelligence.
        </animated.h1>
        <span style={{ fontSize: 30, marginTop: 40 }}>
          EXPLORE OUR CAPABILITIES
        </span>
      </div>

      <div style={{ marginTop: 65 }}>
        {data.map(({ title, text }, index) => {
          return <Container title={title} text={text} index={index} />;
        })}
      </div>
    </div>
  );
}

const Container = ({ title, text, index }) => {
  const [isHovingReadMore, setIsHovingReadMore] = useState(false);

  const imgToRight = index % 2 !== 0;
  return (
    <div
      style={{
        display: "flex",
        flexDirection: imgToRight ? "row" : "row-reverse",
        height: 530,
        color: colors.white,
      }}
    >
      <img
        src={`/operationsTech${index + 1}.webp`}
        style={{
          flex: 1,
          height: "100%",
          objectFit: "cover",
        }}
        alt={`operations_${index}`}
      />

      <div
        style={{
          flex: 1,
          background: `linear-gradient(to right, ${colors.purple} , ${colors.lightPurple})`,
          display: "flex",
          justifyContent: imgToRight ? "flex-start" : "flex-end",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            width: "50%",
            margin: imgToRight ? "70px 0 0 50px" : "70px 50px 0 0",
          }}
        >
          <FontAwesomeIcon
            icon="fa-solid fa-cloud"
            size="2x"
            color={colors.green}
            style={{ border: `1px ${colors.green} 300ms` }}
          />

          <h2 style={{ fontSize: 30, marginTop: 25 }}>{title}</h2>
          <p style={{ marginTop: 45, fontSize: 15, lineHeight: 1.5 }}>{text}</p>
          <div
            style={{
              marginTop: 55,
              height: 3,
              borderRadius: 8,
              width: 180,
              backgroundColor: colors.white,
            }}
          />

          <b
            onMouseEnter={() => setIsHovingReadMore(true)}
            onMouseLeave={() => setIsHovingReadMore(false)}
            style={{
              transition: "100ms letter-spacing",
              fontSize: 13,
              letterSpacing: isHovingReadMore ? 2 : 0,
              cursor: "pointer",
              marginTop: 50,
            }}
          >
            read more
          </b>
        </div>
      </div>
    </div>
  );
};
