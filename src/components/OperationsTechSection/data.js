const data = [
  {
    title: "consulting",
    text: "Accelerate your digital journey with a trusted partner that positions you to capitalize on future-ready opportunities. We reinvent, secure, and accelerate your transformational journey to sustainable growth, diversification, and profitability.",
  },
  {
    title: "Data & Software Technology",
    text: "Explore and leverage the best-in-class, innovative software and technologies with targeted operating models that offer a broad scope of data intelligence, and digitization. Radix helps clients achieve incremental growth, turning challenges into opportunities with the most cost-effective technology investments to empower your workforce.",
  },
  {
    title: "Engineering",
    text: "Ensure optimal efficiency and collaborate throughout your entire life cycle utilizing our suite of engineering solutions to help you scale and achieve operational excellence. From economic analysis to decommissioning, Radix helps re-engineer, design, integrate, and implement solutions that solve the most complex challenges.",
  },
  {
    title: "Operations Technology",
    text: "Maximize speed to value and optimize your entire operations with the power of your data. Leverage industrial automation technologies, gain greater ROI with actionable insights to operate effectively, break down silos, reduce cost, and increase operational efficiency.",
  },
];

export default data;
