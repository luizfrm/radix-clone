import { animated, useSpring, useTrail } from "@react-spring/web";
import colors from "../../colors";
import { useInView } from "react-intersection-observer";

const VALUES_MARGIN = "10vw";

export default function ValuesPart() {
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0.1,
  });

  return (
    <section
      style={{
        display: "flex",
        justifyContent: "center",
        marginLeft: "-" + VALUES_MARGIN,
        marginTop: 110,
      }}
      ref={ref}
    >
      <ValueContainer value={"155+"} text="Valued Customers" inView={inView} />
      <ValueContainer
        value={"3,931+"}
        text="Valued Customers"
        inView={inView}
      />
      <ValueContainer
        value={"1,700+"}
        text="Valued Customers"
        inView={inView}
      />
      <ValueContainer value={"30+"} text="Valued Customers" inView={inView} />
    </section>
  );
}

const ValueContainer = ({ value, text, inView }) => {
  const valueChars = value.split("");
  const textChars = text.split("");

  const trailValue = useTrail(valueChars.length, {
    from: { opacity: 0 },
    to: { opacity: inView ? 1 : 0 },
    delay: 100,
  });

  const trailText = useTrail(textChars.length, {
    from: { opacity: 0 },
    to: { opacity: 1 },
    delay: 100,
  });

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        color: colors.purple,
        marginLeft: VALUES_MARGIN,
        alignItems: "center",
      }}
    >
      <animated.span
        style={{ fontSize: 60, textShadow: "5px 5px 5px rgba(0, 0, 0, 0.3)" }}
      >
        {trailValue.map((props, index) => (
          <animated.span key={index} style={{ ...props }}>
            {inView && valueChars[index] === " " ? "\u00A0" : valueChars[index]}
          </animated.span>
        ))}
      </animated.span>

      <animated.span style={{ fontSize: 18, marginTop: 30 }}>
        {trailText.map((props, index) => (
          <animated.span key={index} style={{ ...props }}>
            {textChars[index] === " " ? "\u00A0" : textChars[index]}
          </animated.span>
        ))}
      </animated.span>

      <animated.div
        style={{
          width: "100%",
          height: 2,
          borderRadius: 5,
          backgroundColor: colors.orange,
          marginTop: 40,
          transformOrigin: "left center",
          ...useSpring({
            from: { transform: "scaleX(0)" },
            to: { transform: "scaleX(1)" },
            config: { duration: valueChars.length * 100 },
          }),
        }}
      />
    </div>
  );
};
