const solutionsData = [
  {
    title: "AGILITY & INNOVATION",
    subTitle:
      "Harness the power of ground-breaking technologies to help accelerate your transforational journey.",
    text: "Harness the power of ground-breaking technologies to help accelerate your transformational journey. Radix provides universal access and resources that help accelerate your engineering ventures, increase cash flow generation, and enable your portfolio diversification.",
  },
  {
    title: "OPERATIONAL EXCELLENCE",
    subTitle:
      "Achieve operational excellence with Radix as your guiding partner. From optimizing your entire process...",
    text: "Achieve operational excellence with Radix as your guiding partner. From optimizing your entire process, increasing workforce efficiency, asset performance management, to predictive maintenance and risk assessments, we equip you with the right technology tools to scale",
  },
  {
    title: "SUSTAINABLE GROWTH",
    subTitle:
      "Create sustainable, economic business value for all stakeholders with a clear path. From concept to design...",
    text: "Create sustainable, economic business value for all stakeholders with a clear path. From concept to design, strategy to execution, reducing risks, to asset reliability for energy efficiency. Radix helps you capitalize on opportunities that meet future-ready needs to reach your sustainability goals.",
  },
];

export default solutionsData;
