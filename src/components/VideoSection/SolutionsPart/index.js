import { useState } from "react";
import colors from "../../../colors";
import data from "./data";

export default function SolutionsPart() {
  return (
    <section
      style={{
        marginTop: 90,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <h1 style={{ color: colors.highlightPurple, fontSize: 35 }}>
        Transformative Solutions{" "}
        <span style={{ color: colors.black }}>
          to Unlock Your Potential Today.
        </span>
      </h1>

      <div
        style={{
          marginTop: 130,
          display: "flex",
          alignItems: "center",
          marginLeft: 32,
        }}
      >
        {data.map(({ title, subTitle, text }, i) => (
          <SolutionContainer
            title={title}
            subTitle={subTitle}
            text={text}
            index={i}
          />
        ))}
      </div>
    </section>
  );
}

const SolutionContainer = ({ title, subTitle, text, index }) => {
  const [isHoving, setIsHoving] = useState(false);
  return (
    <div
      onMouseEnter={() => setIsHoving(true)}
      onMouseLeave={() => setIsHoving(false)}
      style={{
        position: "relative",
        height: 410,
        width: 360,
        transform: isHoving ? "scale(0.90)" : "scale(1)",
        transition: "transform 300ms",
        marginLeft: 32,
        cursor: "default",
        backgroundImage: `url(/solution${index + 1}.webp)`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        borderRadius: "0px 0px 8px 8px",
      }}
    >
      <div
        style={{
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          backgroundColor: isHoving ? "rgba(42, 24, 68, .8)" : colors.purple,
          height: isHoving ? "70%" : 100,

          borderRadius: 8,
          padding: "20px",
          transition: "height 300ms, ",
        }}
      >
        {!isHoving && (
          <div>
            <h2 style={{ color: colors.green }}>{title}</h2>
            <p style={{ color: colors.white, marginTop: 10, lineHeight: 1.5 }}>
              {subTitle}
            </p>
          </div>
        )}
        {isHoving && (
          <div>
            <p style={{ color: colors.white, marginTop: 10, lineHeight: 2 }}>
              {text}
            </p>
          </div>
        )}
      </div>
    </div>
  );
};
