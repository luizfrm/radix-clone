import VideoPart from "./VideoPart";
import ValuesPart from "./ValuesPart";
import SolutionsPart from "./SolutionsPart";

export default function VideoSection() {
  return (
    <div>
      <VideoPart />
      <ValuesPart />
      <SolutionsPart />
    </div>
  );
}
