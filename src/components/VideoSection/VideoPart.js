import { animated, useSpring } from "@react-spring/web";
import colors from "../../colors";
import { useInView } from "react-intersection-observer";

export default function VideoPart() {
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0.1,
  });

  const videoSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(0, 50%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(0, 50%, 0)",
    },
    config: { duration: 800 },
  });

  const titleSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(100%, 0, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(100%, 0, 0)",
    },
    config: { duration: 800 },
  });

  return (
    <section
      style={{
        marginTop: 90,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
      ref={ref}
    >
      <div
        style={{
          display: "flex",
        }}
      >
        <div style={{ flex: 1 }}>
          <animated.video
            autoPlay
            muted
            controls
            style={{
              width: "435px",
              height: "245px",
              boxShadow: "10px 10px 10px rgba(0, 0, 0, 0.5)",
              ...videoSpring,
            }}
          >
            <source src="/sectionVideo.mp4" type="video/mp4" />
            Your browser does not support the video tag.
          </animated.video>
        </div>

        <div
          style={{ marginLeft: 30, width: 500, color: colors.purple, flex: 1 }}
        >
          <animated.h2
            style={{
              fontSize: 30,
              ...titleSpring,
            }}
          >
            Technology Solutions to Turn Your Challenges Into Opportunities
          </animated.h2>
          <p style={{ fontSize: 17, marginTop: 5, lineHeight: "145%" }}>
            Radix is a global, technology solutions company that delivers the
            most innovative, best-in-class industry solutions to scale and
            accelerate your digital transformation journey. With a people-first
            integrity approach, we unify the most intelligent engineers with
            technology and go beyond the creation and implementation of projects
            to solve the most complex challenges.
          </p>
        </div>
      </div>
    </section>
  );
}
