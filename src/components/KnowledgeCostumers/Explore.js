import { useEffect, useRef, useState, useCallback } from "react";
import colors from "../../colors";
import exploreData from "./exploreData";
import { animated, useSpring } from "@react-spring/web";

export default function Explore() {
  return (
    <section style={{ marginTop: 110 }}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          color: colors.purple,
        }}
      >
        <h1 style={{ fontSize: 50 }}>Explore our Knowledge Hub</h1>
        <span style={{ marginTop: 30, fontSize: 20 }}>
          Latest Resources and Success Stories from Radix
        </span>
      </div>
      <Carousel />
    </section>
  );
}

const Carousel = () => {
  const [page, setPage] = useState(0);
  const [delayedPage, setDelayedPage] = useState(0);
  const [isChangingPageToPos, setIsChangingPageToPos] = useState("r");

  const setPageWithDelay = useCallback(
    (newPage) => {
      setDelayedPage(newPage);
      setTimeout(() => {
        setPage(newPage);
        setIsChangingPageToPos("");
      }, 500);
    },
    [setPage]
  );

  useEffect(() => {
    setDelayedPage(delayedPage);
    setIsChangingPageToPos(delayedPage > page ? "r" : "l");
  }, [delayedPage]);

  const isMounted = useRef(false);

  useEffect(() => {
    if (isMounted.current) {
      const interval = setInterval(() => {
        setPageWithDelay((oldPage) => {
          return oldPage == 2 ? 0 : oldPage + 1;
        });
      }, 3000);

      return () => clearInterval(interval);
    }

    isMounted.current = true;
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: 110,
      }}
    >
      <CarouselPage page={page} isChangingPageToPos={isChangingPageToPos} />
      <CarouselNav page={page} setPage={setPageWithDelay} />
    </div>
  );
};

const CarouselPage = ({ page, isChangingPageToPos }) => {
  const [highestHeight, setHighestHeight] = useState(0);

  useEffect(() => {
    setHighestHeight(
      exploreData.reduce((max, obj) => {
        return obj.height > max ? obj.height : max;
      }, 0)
    );
  }, []);

  return (
    <div
      style={{
        display: "flex",
        marginLeft: -25,
        height: highestHeight,
        alignItems: "flex-end",
        marginRight:
          isChangingPageToPos == ""
            ? ""
            : isChangingPageToPos == "r"
            ? "100%"
            : "-100%",
        transform:
          isChangingPageToPos == ""
            ? ""
            : isChangingPageToPos == "r"
            ? "translateX(-50%)"
            : "translateX(50%)",
        transition:
          isChangingPageToPos != "" ? "margin 500ms, transform 100ms" : "",
      }}
    >
      <CarouselItem
        page={page}
        index={0}
        isChangingPageToPos={isChangingPageToPos}
      />
      <CarouselItem
        page={page}
        index={1}
        isChangingPageToPos={isChangingPageToPos}
      />
      <CarouselItem
        page={page}
        index={2}
        isChangingPageToPos={isChangingPageToPos}
      />
    </div>
  );
};

const CarouselItem = ({ page, index, isChangingPageToPos }) => {
  const [isHovingReadMore, setIsHovingReadMore] = useState(false);
  const [lastChangePos, setLastChangePos] = useState("r");

  useEffect(() => {
    if (isChangingPageToPos != "") setLastChangePos(isChangingPageToPos);
  }, [isChangingPageToPos]);

  const { title, text, width, height } = exploreData[page * 3 + index];

  const [springProps, setSpringProps] = useSpring(() => ({
    from: {
      opacity: 0,
      transform: `translate3d(${
        lastChangePos == "r" ? "100%" : "-100%"
      }, 0, 0)`,
    },
    to: {
      opacity: 1,
      transform: "translate3d(0, 0, 0)",
    },
    config: { duration: 800 },
  }));

  useEffect(() => {
    setSpringProps({
      from: {
        opacity: 0,
        transform: `translate3d(${
          lastChangePos == "r" ? "100%" : "-100%"
        }, 0, 0)`,
      },
      to: {
        opacity: 1,
        transform: "translate3d(0, 0, 0)",
      },
      config: { duration: 800 },
    });
  }, [page, lastChangePos]);
  return (
    <animated.div
      style={{
        padding: 20,
        backgroundColor: colors.purple,
        marginLeft: 25,
        width,
        height,
        color: colors.white,
        display: "flex",
        flexDirection: "column",
        boxShadow: "10px 10px 10px rgba(0, 0, 0, 0.5)",
        borderRadius: 8,
        ...springProps,
      }}
    >
      <img
        src={`/carousel/${page + 1}-${index + 1}.webp`}
        alt={`carousel content page: ${page + 1} position: ${index + 1}`}
      />
      <h2
        style={{
          color: colors.green,
          fontSize: 20,
          marginTop: 20,
          lineHeight: 1.5,
        }}
      >
        {title}
      </h2>
      <p style={{ marginTop: 20, fontSize: 16, lineHeight: 1.5 }}>{text}</p>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginTop: "auto",
        }}
      >
        <div
          style={{
            height: 2,
            backgroundColor: colors.white,
            borderRadius: 4,
            flex: 1,
          }}
        />

        <b
          onMouseEnter={() => setIsHovingReadMore(true)}
          onMouseLeave={() => setIsHovingReadMore(false)}
          style={{
            padding: "0px 30px",
            transition: "100ms letter-spacing",
            fontSize: 13,
            letterSpacing: isHovingReadMore ? 2 : 0,
            cursor: "pointer",
          }}
        >
          read more
        </b>
      </div>
    </animated.div>
  );
};

const ballStyle = (isSelected) => ({
  cursor: "pointer",
  backgroundColor: isSelected ? colors.white : colors.purple,
  border: isSelected ? `3px ${colors.purple} solid` : "",
  width: 8,
  height: 8,
  borderRadius: 20,
  marginLeft: 20,
  transition: "border-size 100ms",
});

const CarouselNav = ({ page, setPage }) => {
  return (
    <div
      style={{
        display: "flex",
        marginLeft: -20,
        marginTop: 30,
        alignItems: "center",
      }}
    >
      <div style={ballStyle(0 === page)} onClick={() => setPage(0)} />
      <div style={ballStyle(1 === page)} onClick={() => setPage(1)} />
      <div style={ballStyle(2 === page)} onClick={() => setPage(2)} />
    </div>
  );
};
