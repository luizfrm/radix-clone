import CostumersList from "./CostumersList";
import Explore from "./Explore";

export default function KnowledgeCostumers() {
  return (
    <div>
      <CostumersList />
      <Explore />
    </div>
  );
}
