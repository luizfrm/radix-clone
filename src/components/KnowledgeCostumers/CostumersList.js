import { useEffect, useState } from "react";
import { animated, useSpring } from "@react-spring/web";

export default function CostumersList() {
  return (
    <div style={{ marginTop: 50 }}>
      <ValuedCostumersList />
    </div>
  );
}

const ValuedCostumersList = () => {
  const [imgIndex, setImgIndex] = useState(1);

  useEffect(() => {
    const interval = setInterval(() => {
      setImgIndex((prevIndex) => (prevIndex >= 3 ? 1 : prevIndex + 1));
    }, 4000);

    return () => clearInterval(interval);
  }, []);

  return (
    <section style={{ display: "flex", margin: "0px 180px" }}>
      <div style={{ display: "flex", flexDirection: "column", width: 200 }}>
        <h2 style={{ fontSize: 40 }}>Stronger, Together.</h2>
        <span style={{ marginTop: 30, fontSize: 17 }}>
          Our Valued Customers
        </span>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          width: "100%",
        }}
      >
        <Logo logoPos={1} imgIndex={imgIndex} />
        <Logo logoPos={2} imgIndex={imgIndex} />
        <Logo logoPos={3} imgIndex={imgIndex} />
        <Logo logoPos={4} imgIndex={imgIndex} />
      </div>
    </section>
  );
};

const Logo = ({ imgIndex, logoPos }) => {
  const [currentImgIndex, setCurrentImgIndex] = useState(imgIndex);

  const [props, set] = useSpring(() => ({
    opacity: 1,
    transform: "translate3d(0, 0, 0)",
    config: { duration: 1000 },
  }));

  useEffect(() => {
    set({
      opacity: 0,
      transform: "translate3d(100%, 0, 0)",
      onRest: () => {
        setCurrentImgIndex(imgIndex);
        set({ opacity: 1, transform: "translate3d(0, 0, 0)" });
      },
    });
  }, [imgIndex, set]);

  return (
    <animated.img
      style={{
        width: "auto",
        height: "auto",
        ...props,
      }}
      src={`/logos/${logoPos}-${currentImgIndex}.webp`}
      alt={`logos_${currentImgIndex}`}
    />
  );
};
