const data = [
  {
    title: "How to Achieve Data Driven Process Optimization in Pulp and Paper",
    text: "The Pulp and paper industry is historically an industry that boosts production by increasing operational utilization rateshanging Pulp & Paper.'' Contact www.radixeng.com to learn more.",
    height: 580,
    width: 300,
  },
  {
    title:
      "Process Digital Twins: Gamechangers for Mill-wide Optimization in Pulp and Paper",
    text: "Pulp and Paper mills across the globe are facing the challenge of being more productive and delivering more quality while minimizing and optimizing costs. In a scenario where companies seek to provide solutions without large investments, using existing facilities most efficiently becomes not only desirable but necessary.",
    height: 600,
    width: 330,
  },
  {
    title: "Achieve Game-Changing Optimization in Pulp & Paper with AI and APC",
    text: `Demystify #artificialintelligence by listening to this quick seminar on "How AI and APC are changing Pulp & Paper.'' Contact www.radixeng.com to learn more.`,
    height: 520,
    width: 300,
  },

  {
    title: "Boosting Your Approach Towards Manufacturing Optimization",
    text: "As profit margins tighten due to inflation and supply chain stress, improving efficiency has become a necessity in almost every industry.",
    height: 470,
    width: 270,
  },
  {
    title: "The Path to Profits with Digital Twins",
    text: "Digital twin technology has a dizzying array of use cases, benefits, and approaches. Many now understand the benefits of the technology, ranging from increased efficiency and revenue to reduced downtime and costs.",
    height: 550,
    width: 340,
  },
  {
    title:
      "Boost Predictive Maintenance in Thermal Power Plants with Digital Twins",
    text: "The challenge was to develop a decision support system via trend prediction, operational deviations, and fuel qualification.",
    height: 430,
    width: 250,
  },

  {
    title: "Predictive Maintenance in Downstream",
    text: "One of the main players in the chemical and petrochemical sector worldwide needed to make the transition from a preventive maintenance model, with a pre-established frequency for the downtime of its equipment, to a predictive maintenance model, in which intelligent solutions would be able to inform in advance eventual failures, promoting a major cultural change in the company.",
    height: 610,
    width: 340,
  },
  {
    title: "Data Aggregation Program in Upstream",
    text: "The program’s purpose was to define the requirements for connecting data from different sources into a single source of truth – the Data Aggregator. As a digital twin hub in four assets in different stages of the lifecycle.",
    height: 520,
    width: 260,
  },
  {
    title: "How to Achieve Data-Driven Process Optimization in Pulp and Paper",
    text: "Applying the power of data to the pulp and paper (P&P) industry is a game-changing approach towards potentially driving profits and minimizing downtime. ",
    height: 520,
    width: 320,
  },
];

export default data;
