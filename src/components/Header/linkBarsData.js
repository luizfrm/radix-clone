const barContents = {
  solutions: [
    "Consulting",
    "Education",
    "Energy",
    "Infrastructure & Logistics",
    "Manufacturing",
  ],
  industries: ["Agriculture & AgriBusiness", "Food & Beverage"],
  newsAndInsights: [
    "News & Highlights",
    "Industry Trends",
    "Radix Resources",
    "Events",
  ],
  aboutUs: [
    "About us",
    "Radix EveryWhere",
    "Strategic Partnerships",
    "Diversity, Ethics & Integrity",
    "Protection of Personal Data",
  ],
};

export default barContents;
