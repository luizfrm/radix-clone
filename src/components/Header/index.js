import { useState } from "react";
import colors from "../../colors";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./index.css";
import linkBarsData from "./linkBarsData";

const linksStyle = { marginLeft: 30 };

export default function Header() {
  return (
    <header
      style={{
        position: "fixed",
        display: "flex",
        justifyContent: "space-between",
        padding: "20px 20px",
        background: `linear-gradient(to bottom, ${colors.purple} 10%, rgba(42, 24, 68, 0.8) 60%, rgba(42, 24, 68, 0.6) 80%, transparent 100%)`,
        color: colors.white,
        right: 0,
        left: 0,
        zIndex: 10,
      }}
    >
      <img src="/logo.webp" alt="radix's logo" style={{ cursor: "pointer" }} />
      <nav
        style={{
          fontSize: 13,
          display: "flex",
          alignItems: "center",
        }}
        onClick={() => alert("apenas a home foi copiada")}
      >
        <LinkAndBar linkNames={linkBarsData.solutions}>
          <a href="/">Solutions</a>
        </LinkAndBar>
        <LinkAndBar linkNames={linkBarsData.industries} style={linksStyle}>
          <a href="/">Industries</a>
        </LinkAndBar>
        <LinkAndBar linkNames={linkBarsData.aboutUs} style={linksStyle}>
          <a href="/">News & Insights</a>
        </LinkAndBar>

        <a href="/" style={linksStyle}>
          Careers
        </a>
        <a href="/" style={linksStyle}>
          Contact
        </a>
      </nav>
      <SearchBar />
    </header>
  );
}

const LinkAndBar = ({ children, linkNames, style }) => {
  const [isBarVisible, setIsBarVisible] = useState(false);

  return (
    <div
      onMouseEnter={() => setIsBarVisible(true)}
      onMouseLeave={() => setIsBarVisible(false)}
      style={{
        position: "relative",
        ...style,
      }}
    >
      {children}

      {isBarVisible && (
        <div
          style={{
            position: "absolute",
            left: "50%",
            transform: "translateX(calc(-50%))",
          }}
        >
          <div style={{ height: 30 }} />

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              color: colors.black,
              alignItems: "center",
              padding: 10,
            }}
          >
            {linkNames.map((name) => (
              <BarLink name={name} />
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

const BarLink = ({ name }) => {
  const [isHoving, setIsHoving] = useState(false);

  return (
    <span
      onMouseEnter={() => setIsHoving(true)}
      onMouseLeave={() => setIsHoving(false)}
      style={{
        padding: 10,
        backgroundColor: isHoving ? colors.purple : colors.white,
        color: isHoving ? colors.white : colors.black,
        width: 200,
        textAlign: "center",
        fontSize: 16,
        cursor: "pointer",
      }}
    >
      {name}
    </span>
  );
};

const SearchBar = () => {
  const [searchText, setSearchText] = useState();

  return (
    <div style={{ display: "flex" }}>
      <input
        value={searchText}
        onChange={(ev) => setSearchText(ev.target.value)}
        style={{
          borderRadius: "80px 0px 0px 80px",
          width: 100,
          paddingLeft: 10,
        }}
        placeholder="Search..."
      />
      <button
        style={{
          backgroundColor: colors.lightPurple,
          borderRadius: "0px 80px 80px 0px",
          padding: "0px 5px",
          display: "flex",
          alignItems: "center",
        }}
        onClick={() => alert(searchText)}
      >
        <FontAwesomeIcon
          icon="fa-solid fa-magnifying-glass"
          size="1x"
          color="white"
        />
      </button>
    </div>
  );
};
