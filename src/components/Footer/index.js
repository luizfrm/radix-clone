import colors from "../../colors";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./index.css";

const visitRadixWidth = 200;

export default function Footer() {
  return (
    <footer
      style={{
        backgroundColor: colors.purple,
        color: colors.white,
        padding: 50,
        fontSize: 15,
      }}
    >
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <img
          src="/bigLogo.webp"
          alt="radix's logo"
          style={{ cursor: "pointer", height: 65, width: 170 }}
        />
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            width: visitRadixWidth,
          }}
        >
          <img
            src="/brazilFlag.webp"
            alt="Brazil's flag"
            style={{ cursor: "pointer", height: 25, width: 35 }}
          />
          <AnimatedSpan
            isLetterSpacing={true}
            fontSize={15}
            style={{
              fontWeight: "bold",
              color: colors.green,
              marginLeft: 5,
              fontSize: 18,
            }}
          >
            Visit Radix BR
          </AnimatedSpan>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <LocationsContainer />
        <ContactContainer />
        <MailContainer />
        <SocialMedia />
      </div>
    </footer>
  );
}

const LocationsContainer = () => {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <span style={{ marginTop: 50, width: 300, fontSize: 18 }}>
        Please email any questions or comments to
        <span style={{ textDecoration: "underline" }}>
          {" "}
          contact@radixeng.com.
        </span>
        <div>
          <h3 style={{ fontSize: 20, marginTop: 45 }}>Locations</h3>
          <div
            style={{
              display: "flex",
              marginTop: 15,
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "space-between",
                width: 160,
                height: 100,
              }}
            >
              <AnimatedSpan
                isLetterSpacing={true}
                isFontIncreasing={true}
                fontSize={15}
                style={{ marginTop: 20, fontWeight: "bold" }}
              >
                Houston
              </AnimatedSpan>
              <AnimatedSpan
                isLetterSpacing={true}
                isFontIncreasing={true}
                fontSize={15}
                style={{ marginTop: 20, fontWeight: "bold" }}
              >
                Belo Horizonte
              </AnimatedSpan>
              <AnimatedSpan
                isLetterSpacing={true}
                isFontIncreasing={true}
                fontSize={15}
                style={{ marginTop: 20, fontWeight: "bold" }}
              >
                Amsterdam
              </AnimatedSpan>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <AnimatedSpan
                isLetterSpacing={true}
                isFontIncreasing={true}
                fontSize={15}
                style={{ marginTop: 20, fontWeight: "bold" }}
              >
                Rio de Janeiro
              </AnimatedSpan>
              <AnimatedSpan
                isLetterSpacing={true}
                isFontIncreasing={true}
                fontSize={15}
                style={{ marginTop: 20, fontWeight: "bold" }}
              >
                São Paulo
              </AnimatedSpan>
            </div>
          </div>
        </div>
      </span>
    </div>
  );
};

const AnimatedSpan = ({
  children,
  fontSize,
  isLetterSpacing,
  isFontIncreasing,
  style,
}) => {
  const [isHoving, setIsHoving] = useState(false);
  return (
    <span
      onMouseEnter={() => setIsHoving(true)}
      onMouseLeave={() => setIsHoving(false)}
      style={{
        fontSize: isFontIncreasing
          ? isHoving
            ? fontSize * 1.1
            : fontSize
          : fontSize,
        letterSpacing: isLetterSpacing ? (isHoving ? "2px" : 0) : "",
        transition: "font-size 300ms, letter-spacing 300ms",
        cursor: "pointer",
        textDecoration: isHoving ? "underline" : "none",
        ...style,
      }}
    >
      {children}
    </span>
  );
};

const ContactContainer = () => {
  const [isHovingButton, setIsHovingButton] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Services
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Industries
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          About Us
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Resources
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Privacy
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Accessibility
        </AnimatedSpan>
        <AnimatedSpan
          isLetterSpacing={true}
          fontSize={15}
          style={{ marginTop: 15 }}
        >
          Sitemap
        </AnimatedSpan>

        <button
          onMouseEnter={() => setIsHovingButton(true)}
          onMouseLeave={() => setIsHovingButton(false)}
          style={{
            marginTop: 35,
            padding: 10,
            backgroundColor: isHovingButton
              ? colors.purple
              : colors.lightPurple,
            color: colors.white,
            borderRadius: isHovingButton ? 50 : 10,
            fontStyle: isHovingButton ? "italic" : "normal",
            border: isHovingButton
              ? `2px ${colors.green} solid`
              : `2px ${colors.lightPurple} solid`,
            transition: "border-radius 100ms, border 100ms, fontStyle 100ms",
            letterSpacing: 4,
          }}
        >
          <b>Contact</b>
          <FontAwesomeIcon
            icon="fa-solid fa-right-long"
            size="1x"
            color={isHovingButton ? colors.green : colors.white}
            style={{ marginLeft: 5 }}
          />
        </button>
      </div>
    </div>
  );
};
const MailContainer = () => {
  const [isHovingButton, setIsHovingButton] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        marginTop: 30,
        alignItems: "center",
      }}
    >
      <div style={{ display: "flex", flexDirection: "column" }}>
        <b style={{ fontSize: 18 }}>Stay connected with us & subscribe.</b>
        <input
          style={{
            marginTop: 30,
            padding: 15,
            borderRadius: 4,
            border: `1px ${colors.white} solid`,
            backgroundColor: colors.purple,
            color: colors.white,
          }}
          placeholder="Email Address (i.e. info@xyz.com)"
        />

        <div>
          <button
            onMouseEnter={() => setIsHovingButton(true)}
            onMouseLeave={() => setIsHovingButton(false)}
            style={{
              marginTop: 10,
              padding: "5px 20px",
              backgroundColor: isHovingButton ? colors.white : colors.green,
              color: isHovingButton ? colors.lightPurple : colors.purple,
              borderRadius: 4,
              transition: "color 300ms, background 300ms",
              fontSize: 20,
            }}
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
const SocialMedia = () => {
  return (
    <div
      style={{
        marginTop: 30,
        display: "flex",
        flexDirection: "column",
        // alignItems: "center",
        width: visitRadixWidth,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <b style={{ fontSize: 22 }}>Follow Us On:</b>

        <div style={{ marginTop: 30, display: "flex" }}>
          <img
            alt="instagram_icon"
            src={"/instagram.webp"}
            width={35}
            height={35}
          />
          <img
            alt="linkedin_icon"
            src={"/linkedin.webp"}
            width={35}
            height={35}
            style={{ marginLeft: 10 }}
          />
          <img
            alt="youtube_icon"
            src={"/youtube.webp"}
            width={35}
            height={35}
            style={{ marginLeft: 10 }}
          />
        </div>
      </div>
    </div>
  );
};
