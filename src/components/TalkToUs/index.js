import { useState } from "react";
import colors from "../../colors";
import { animated, useSpring } from "@react-spring/web";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useInView } from "react-intersection-observer";

export default function TalkToUs() {
  const [isHovingButton, setIsHovingButton] = useState(false);
  const [ref, inView] = useInView({
    triggerOnce: false,
    threshold: 0.3,
  });

  const lineSpring = useSpring({
    from: { opacity: 0, width: "0px" },
    to: { opacity: inView ? 1 : 0, width: inView ? "50px" : "0px" },
    config: { duration: 800 },
  });

  const buttonSpring = useSpring({
    from: { opacity: 0, transform: "translate3d(0, 200%, 0)" },
    to: {
      opacity: inView ? 1 : 0,
      transform: inView ? "translate3d(0, 0, 0)" : "translate3d(0, 200%, 0)",
    },
    config: { duration: 800 },
  });

  return (
    <section style={{ display: "flex", marginTop: 115 }} ref={ref}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          flex: 0.45,
        }}
      >
        <div>
          <h1
            style={{
              fontSize: 50,
              color: colors.purple,
              width: 500,
            }}
          >
            Unlock Your Potential with Radix
          </h1>
          <animated.div
            style={{
              marginTop: 40,
              width: 50,
              height: 4,
              borderRadius: 4,
              backgroundColor: colors.green,
              ...lineSpring,
            }}
          />
          <animated.button
            onMouseEnter={() => setIsHovingButton(true)}
            onMouseLeave={() => setIsHovingButton(false)}
            style={{
              marginTop: 50,
              padding: 25,
              backgroundColor: isHovingButton
                ? colors.white
                : colors.lightPurple,
              color: isHovingButton ? colors.purple : colors.white,
              borderRadius: isHovingButton ? 50 : 20,
              fontStyle: isHovingButton ? "italic" : "normal",
              border: isHovingButton
                ? `2px ${colors.green} solid`
                : `2px ${colors.lightPurple} solid`,
              transition: "border-radius 100ms, border 100ms, fontStyle 100ms",
              letterSpacing: 4,
              ...buttonSpring,
            }}
          >
            <span>Talk to an Expert</span>
            <FontAwesomeIcon
              icon="fa-solid fa-right-long"
              size="1x"
              color={isHovingButton ? colors.purple : colors.white}
              style={{ marginLeft: 5 }}
            />
          </animated.button>
        </div>
      </div>

      <img
        src="/talkToUs.webp"
        style={{ height: 550, flex: 0.55, objectFit: "cover" }}
      />
    </section>
  );
}
