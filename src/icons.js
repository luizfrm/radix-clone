import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faRightLong,
  faCloud,
} from "@fortawesome/free-solid-svg-icons";

library.add(faMagnifyingGlass, faRightLong, faCloud);
